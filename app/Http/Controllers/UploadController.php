<?php

namespace App\Http\Controllers;

use App\Letture;
use App\Letture_dettaglio;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use function Symfony\Component\String\s;

class UploadController extends Controller
{
    public function store(Request $request){
        if(checkAutorization($request->header('User-Name'), $request->header('Pwd')) == false){
            header('upload: error');
            echo "Response header upload: error - autorizzazione negata."."<br>";
            echo "Response status code: 200"."<br>";
            $data_error = Storage::disk('local')->get('errore_autenticazione.xml');
            echo $data_error;
            die();
        }
        $data = file_get_contents('php://input');
        //$risp=file_put_contents('invio_intervento_'.date("Ymd")."_".date("His").'.xml', $data);
        $xml=simplexml_load_string($data);
        $xml->registerXPathNamespace('c','http://www.softecspa.it/checktrack');
        foreach($xml->intervento as $intervento){
            $codice = StripTrim($intervento->codice);
            $cod_operatore = StripTrim($intervento->cod_operatore);
            $destinazione = StripTrim($intervento->destinazione);
            $cod_tipo_int = StripTrim($intervento->cod_tipo_int);
            $tel = StripTrim($intervento->tel);
            $result=$xml->xpath('//c:parametro');
            foreach ($result as $parametro){
                $parametro_nome = $parametro->nome;
                switch ($parametro_nome){
                    case "dsser":
                        $dsser = StripTrim($parametro->valore);
                        break;
                    case "pperpri":
                        $pperpri = StripTrim($parametro->valore);
                        break;
                    case "pute":
                        $pute = StripTrim($parametro->valore);
                        break;
                    case "ragsoc":
                        $ragsoc = StripTrim($parametro->valore);
                        break;
                    case "telef":
                        $telef = StripTrim($parametro->valore);
                        break;
                    case "cell":
                        $cell = StripTrim($parametro->valore);
                        break;
                    case "dsutil":
                        $dsutil = StripTrim($parametro->valore);
                        break;
                    case "pbuolav":
                        $pbuolav = StripTrim($parametro->valore);
                        break;
                    case "rifdom":
                        $rifdom = StripTrim($parametro->valore);
                        break;
                    case "dslav":
                        $dslav = StripTrim($parametro->valore);
                        break;
                    case "nlav":
                        $nlav = StripTrim($parametro->valore);
                        break;
                    case "tlavcnt":
                        $tlavcnt = StripTrim($parametro->valore);
                        break;
                    case "comune":
                        $comune = StripTrim($parametro->valore);
                        break;
                    case "localita":
                        $localita = StripTrim($parametro->valore);
                        break;
                    case "indir":
                        $indir = StripTrim($parametro->valore);
                        break;
                    case "timp":
                        $timp = StripTrim($parametro->valore);
                        break;
                    case "dsubic":
                        $dsubic = StripTrim($parametro->valore);
                        break;
                    case "nstacco":
                        $nstacco = StripTrim($parametro->valore);
                        break;
                    case "cmar":
                        $cmar = StripTrim($parametro->valore);
                        break;
                    case "nmat":
                        $nmat = StripTrim($parametro->valore);
                        break;
                    case "vallet":
                        $vallet = StripTrim($parametro->valore);
                        break;
                    case "ncif":
                        $ncif = StripTrim($parametro->valore);
                        break;
                    case "diam":
                        $diam = StripTrim($parametro->valore);
                        break;
                    case "dtlet":
                        $dtlet = StripTrim($parametro->valore);
                        break;
                    case "lat":
                        $lat = StripTrim($parametro->valore);
                        break;
                    case "long":
                        $long = StripTrim($parametro->valore);
                        break;
                    case "DSSTATO":
                        $dsstato = StripTrim($parametro->valore);
                        break;
                }
            }
            if ($codice == "" or $dsstato == ""){
                header('upload: error');
                echo "Response header upload: error - Field Codice or dsstato is empty"."<br>";
                echo "Response status code: 200"."<br>";
                $data_error = Storage::disk('local')->get('errore_codice.xml');
                echo $data_error;
                die();
            }

            // se dsstato = confermato devo fare un insert, verificando prima che il dato nopn sia gi� stato inserito
            // se dsstato = annullato devo fare un update verificando che lo stato dell'utenza in GEA4 non sia LET

            $data_invio = date("d/m/Y");
            $ora_invio = date("H:i");
            $codice = doubleval($codice);

            //***************************** INDIRIZZO *****************************************
            $civico = "";
            $barrato = "";
            $posciv = strpos($indir,"n.");
            $n_car_indir = strlen($indir);
            if($posciv>0){
                $n_car_civ = $n_car_indir - ($posciv +2);
                $civico = trim(substr($indir,$posciv+2,$n_car_civ));
                $posbar = strpos($civico,"/");
                if($posbar>0){
                    $n_car_bar = $n_car_civ - $posbar+1;
                    $barrato = trim(substr($civico,$posbar+1,$n_car_bar));
                    $civico = trim(substr($civico,0,$posbar));
                }
                $indir = substr($indir,0,$posciv-1);
            }

            //******************************* TIPO ATTIVITA' e attribuzione a lavorazione ************************************
            $progressivo = "";

            switch (str_replace("'","",strtolower($dslav))){
                case "consegna preavviso distacco":
                    $lavorazione_id = Config::get('app.LAVORAZIONE_ID_PRE');
                    $progressivo = Config::get('app.PROGRESSIVO_PRE');
                    $appalto = Config::get('app.APPALTO_PRE');
                    $codice_ente = Config::get('app.ENTE_PRE');
                    break;
                case "sospensione per morosita con lente":
                case "limitazione per morosita con lente":
                case "sospensione per morosita con kit":
                    $lavorazione_id = Config::get('app.LAVORAZIONE_ID_PIO');
                    $progressivo = Config::get('app.PROGRESSIVO_PIO');
                    $appalto = Config::get('app.APPALTO_PIO');
                    $codice_ente = Config::get('app.ENTE_PIO');
                    break;
                case "verifica manomissione allaccio":
                    $lavorazione_id = Config::get('app.LAVORAZIONE_ID_VER');
                    $progressivo = Config::get('app.PROGRESSIVO_VER');
                    $appalto = Config::get('app.APPALTO_VER');
                    $codice_ente = Config::get('app.ENTE_VER');
                    break;
                case "riapertura contatore limitato":
                case "riapertura contatore sospeso":
                case "riapertura contatore limitato - voltura":
                case "riapertura contatore sospeso voltura":
                    $lavorazione_id = Config::get('app.LAVORAZIONE_ID_RIA');
                    $progressivo = Config::get('app.PROGRESSIVO_RIA');
                    $appalto = Config::get('app.APPALTO_RIA');
                    $codice_ente = Config::get('app.ENTE_RIA');
                    break;
                case "disattivazione fornitura per morosita":
                    $lavorazione_id = Config::get('app.LAVORAZIONE_ID_RIM');
                    $progressivo = Config::get('app.PROGRESSIVO_RIM');
                    $appalto = Config::get('app.APPALTO_RIM');
                    $codice_ente = Config::get('app.ENTE_RIM');
                    break;
                case "reistallazione contatore su moroso":
                    $lavorazione_id = Config::get('app.LAVORAZIONE_ID_REI');
                    $progressivo = Config::get('app.PROGRESSIVO_REI');
                    $appalto = Config::get('app.APPALTO_REI');
                    $codice_ente = Config::get('app.ENTE_REI');
            }
            //************************************************************************************
            if($progressivo == ""){
                header('upload: error');
                echo "Response header upload: error - dslav not found"."<br>";
                echo "Response status code: 200"."<br>";
                die();
            }
            switch ($dsstato){
                case "CONFERMATO":
                    $count = Letture::where('progressivo',$progressivo)
                                    ->whereNotIn('stato',['SPE','ANN'])
                                    ->where('pdr',$codice)->count();
                    if($count > 0){
                        header('upload: error');
                        echo "Response header upload: error - Codice already loaded"."<br>";
                        echo "Response status code: 200"."<br>";
                        $data_error = Storage::disk('local')->get('errore_codice_doppio.xml');
                        echo $data_error;
                        die();
                    }
                    $max_seq = Letture::where('progressivo',$progressivo)->max('sequenza');
                    $sequenza = $max_seq +1;
                    $nota_contatore = substr($dslav." - ".$dsutil,0,100);
                    $extra = FormatStr($cod_operatore,"SD",2).FormatStr($destinazione,"SD",50).FormatStr($cod_tipo_int,"SD",1).FormatStr($dsser,"SD",5).FormatStr($telef,"SD",30).FormatStr($cell,"SD",30).FormatStr($pbuolav,"SD",30).FormatStr($rifdom,"SD",30).FormatStr($tlavcnt,"SD",10).FormatStr($dslav,"SD",100).FormatStr($nlav,"SD",100).FormatStr($cmar,"SD",10);
                    $ubicazione = substr($dsubic." ".$nstacco,0,100);
                    try{
                        $lettura = new Letture;
                        $lettura->lavorazione_id = $lavorazione_id;
                        $lettura->appalto_id = $appalto;
                        $lettura->progressivo = $progressivo;
                        $lettura->sequenza = $sequenza;
                        $lettura->periodicita = 'P';
                        //$lettura->portata = strlen(trim($vallet)) >0 ? $diam : '';
                        $lettura->pdr= $codice;
                        $lettura->codice_utente = $pperpri;
                        $lettura->utente = $ragsoc;
                        $lettura->indirizzo = $indir;
                        $lettura->civico = $civico;
                        $lettura->barrato = substr($barrato,0,5);
                        $lettura->localita = $localita;
                        $lettura->comune = $comune;
                        $lettura->fornitura = 'A';
                        $lettura->matricola = $nmat;
                        $lettura->cifre = $ncif;
                        $lettura->note = "(".$diam.") ".$nota_contatore;
                        $lettura->ubicazione = $ubicazione;
                        $lettura->lettura_precedente_ente = $vallet;
                        $lettura->letmin = strlen(trim($vallet)) >0 ? $vallet : 0;
                        $lettura->letmax = '99999999';
                        $lettura->data_lettura_precedente_ente = $dtlet;
                        $lettura->latitudine = $lat;
                        $lettura->longitudine = $long;
                        $lettura->indice_file = 0;
                        $lettura->codice_ente = $codice_ente;
                        $lettura->progmis = $pute;
                        $lettura->profilo_di_prelievo = $dsstato;
                        $lettura->data_inizio = $data_invio;
                        $lettura->data_fine = $ora_invio;
                        $lettura->extra = $extra;
                        $lettura->chiave_univoca = '';
                        $lettura->ordine_civici = 0;
                        $lettura->misuratore = '';
                        $lettura->save();
                    }catch (\Exception $e){
                        header('upload: error');
                        echo "Response header upload: error - no data load into db - format invalid"."<br>";
                        echo "Response status code: 200"."<br>";
                        $data_error = Storage::disk('local')->get('errore_db.xml');
                        echo $data_error;
                        $e->getMessage();
                        Log::error($e);
                        die();
                    }


                    if($lettura->id > 0){
                        if(strlen(trim($nlav)) > 0 ){
                            $letture_dettaglio = New Letture_dettaglio;
                            $letture_dettaglio->lavorazione_id = $lavorazione_id;
                            $letture_dettaglio->progressivo = $progressivo;
                            $letture_dettaglio->id_lettura = $lettura->id;
                            $letture_dettaglio->codice_ente = $codice_ente;
                            $letture_dettaglio->col1 = $nlav;
                            $letture_dettaglio->save();
                        }
                        header('upload: ok');
                        echo "Response header upload: ok";
                        $data_error = Storage::disk('local')->get('ricezione_xml_ok.xml');
                        echo $data_error;
                        if(substr(strtolower($dslav),0,10) == 'riapertura'){
                            $this->inviaNotifica();
                        }
                        die();
                    }else{
                        header('upload: error');
                        echo "Response header upload: error - no data load into db"."<br>";
                        echo "Response status code: 200"."<br>";
                        $data_error = Storage::disk('local')->get('errore_db.xml');
                        echo $data_error;
                        die();
                    }

                    break;
                case "ANNULLATO":
                    $count = Letture::where('progressivo',$progressivo)
                                    ->where('pdr',$codice)
                                    ->whereIn('stato',['LET','SPE'])->count();
                    if($count > 0){
                        header('upload: error');
                        echo "Response header upload: error - Utenza gia piombata"."<br>";
                        echo "Response status code: 200"."<br>";
                        $data_error = Storage::disk('local')->get('errore_annullamento.xml');
                        echo $data_error;
                        die();
                    }
                    $count1 = Letture::where('progressivo',$progressivo)
                        ->where('pdr',$codice)
                        ->where('profilo_di_prelievo','CONFERMATO')->count();
                    if($count1 = 0){
                        header('upload: error');
                        echo "Response header upload: error - Utenza non trovata"."<br>";
                        echo "Response status code: 200"."<br>";
                        $data_error = Storage::disk('local')->get('errore_annullamento1.xml');
                        echo $data_error;
                        die();
                    }
                    $letture = Letture::where('progressivo',$progressivo)
                        ->whereNotIn('stato',['LET','SPE'])
                        ->where('pdr',$codice)
                        ->update(['stato'=>'ANN','profilo_di_prelievo'=>'ANNULLATO','data_inizio'=>$data_invio, 'data_fine'=>$ora_invio]);

                    if($letture =1){
                        header('upload: ok');
                        echo "Response header upload: ok";
                        $data_error = Storage::disk('local')->get('ricezione_xml_ok.xml');
                        echo $data_error;
                        die();
                    }else{
                        header('upload: error');
                        echo "Response header upload: error - no data load into db"."<br>";
                        echo "Response status code: 200"."<br>";
                        $data_error = Storage::disk('local')->get('errore_db.xml');
                        echo $data_error;
                        die();
                    }
                    break;
            }
        }
    }

    public function inviaNotifica(){
        Mail::raw('Sono arrivate in GEA dei buoni di riattivazione', function ($message) {
            $message->to(config('mail.to'));
            $message->subject('Riattivazione contatore');
        });
    }






}
