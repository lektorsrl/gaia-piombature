<?php

namespace App\Http\Controllers;


use App\Letture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function store(Request $request)
    {

        if (checkAutorization($request->header('User-Name'), $request->header('Pwd')) == false) {
            header('upload: error');
            echo "Response header upload: error - autorizzazione negata." . "<br>";
            echo "Response status code: 200" . "<br>";
            $data_error = Storage::disk('local')->get('errore_autenticazione.xml');
            echo $data_error;
            die();
        }
        $incrementale = $request->header('Incrementale');
        $dataf = $request->header('Data');
        $codice_int = $request->header('Codice-Int');
        $enti = [Config::get('app.ENTE_PIO'), Config::get('app.ENTE_VER'), Config::get('app.ENTE_RIA'), Config::get('app.ENTE_RIM'), Config::get('app.ENTE_REI'), Config::get('app.ENTE_PRE')];
        $data = Storage::disk('local')->get('ricezione_esiti_interventi.xml');
        $xml = simplexml_load_string($data);
        $xml->addChild('data', $dataf);
        $xml->addChild('incrementale', $incrementale);
        $xml->addChild('codice_int', $codice_int);
        $xml->addChild('interventi');

        if ($incrementale == 1) {
            $record = Letture::whereIn('codice_ente', $enti)
                ->where('stato', 'LET')
                ->where('flag_controllata', 'O')
                ->get();
        }
        if ($incrementale == 0) {
            $record = Letture::whereIn('codice_ente', $enti)
                ->whereIN('stato', ['LET', 'SPE'])
                ->where('flag_controllata', 'O')
                ->get();
        }
        if (trim($codice_int) != '') {
            $record = Letture::whereIn('codice_ente', $enti)
                ->whereIN('stato', ['LET', 'SPE'])
                ->where('flag_controllata', 'O')
                ->where('pdr', doubleval($codice_int))
                ->get();
        }
        if (count($record) > 0) {
            foreach ($record as $value) {
                $extra = $value->extra;
                $rifdom = trim(substr($extra, 148, 30));
                $pbuolav = trim(substr($extra, 118, 30));
                $data1 = strtotime(str_replace('/', '-', $value->data_lettura) . " " . $value->ora_lettura);
                $data2 = $value->data_lettura . $value->ora_lettura;
                $data3 = substr($data2, 6, 4) . "-" . substr($data2, 3, 2) . "-" . substr($data2, 0, 2) . "T" . $value->ora_lettura;

                $segn1 = $value->segn1;
                $ctlaves = 'NN';
                $tlavcntes = 'N';
                $stato = $segn1;
                switch ($value->codice_ente) {
                    case "25":
                        if($segn1 == '67' || $segn1== '68' || $segn1== '69'){
                            $ctlaves = $segn1;
                            $stato = '99';
                            if ($segn1 == '67' || $segn1 == '68') {
                                $tlavcntes = 'P';
                            }
                            if ($segn1 == '69') {
                                $tlavcntes = 'IR';
                            }
                        }else{
                            $ctlaves = 'NN';
                            $tlavcntes = 'N';
                            $stato = $segn1;
                        }
                        break;
                    case "26":
                        if($segn1 == 79){
                            $ctlaves = $segn1;
                            $stato = '99';
                            $tlavcntes = 'N';
                        }else{
                            $ctlaves = 'NN';
                            $tlavcntes = 'N';
                            $stato = $segn1;
                        }
                        break;
                    case "27":
                        if($segn1 == '82' || $segn1== '83' || $segn1== '84'|| $segn1== '85'){
                            $ctlaves = $segn1;
                            $stato = '99';
                            if ($segn1 == '82' || $segn1 == '84') {
                                $tlavcntes = 'SP';
                            }
                            if ($segn1 == '83' || $segn1 == '85') {
                                $tlavcntes = 'RR';
                            }
                        }else{
                            $ctlaves = 'NN';
                            $tlavcntes = 'N';
                            $stato = $segn1;
                        }
                        break;
                    case "28":
                        if($segn1 == '65'){
                            $ctlaves = $segn1;
                            $stato = '99';
                            $tlavcntes = 'R';
                        }else{
                            $ctlaves = 'NN';
                            $tlavcntes = 'N';
                            $stato = $segn1;
                        }
                        break;
                    case "29":
                        if ($segn1 == 'am'){
                            $ctlaves = $segn1;
                            $stato = '99';
                            $tlavcntes = 'R';
                        }else{
                            $ctlaves = 'NN';
                            $tlavcntes = 'N';
                            $stato = $segn1;
                        }
                        break;
                    case "33":
                        if($segn1 == 86){
                            $ctlaves = $segn1;
                            $stato = '99';
                            $tlavcntes = 'N';
                        }else{
                            $ctlaves = 'NN';
                            $tlavcntes = 'N';
                            $stato = $segn1;
                        }
                        break;
                    default:
                        $ctlaves = 'NN';
                        $tlavcntes = 'N';
                        $stato = $segn1;
                }

                $user = $xml->interventi->addChild('intervento');
                $user->addattribute('cod', FormatStr($value->pdr, "N", 12));
                $user->addattribute('id', '0');
                $user->addChild('cod_tipo_int', $tlavcntes);
                $user->addChild('cod_operatore', ' ');
                $user1 = $user->addChild('parametri');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'rifdom');
                $user2->addChild('valore', $rifdom);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'pbuolav');
                $user2->addChild('valore', $pbuolav);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'cser');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'pperpri');
                $user2->addChild('valore', $value->codice_utente);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'pute');
                $user2->addChild('valore', $value->progmis);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'ctut');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'comune');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'localita');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'indir');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'nciv');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'ctlaves');
                $user2->addChild('valore', $ctlaves);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'stato');
                $user2->addChild('valore', $stato);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'tlavcntes');
                $user2->addChild('valore', $tlavcntes);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'deseces');
                $user2->addChild('valore', $data1);
                $user2->addChild('timestamp', $data3);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'dtletes');
                $user2->addChild('valore', $data1);
                $user2->addChild('timestamp', $data3);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'nlaves');
                $user2->addChild('valore', $value->nota);
                if ($value->locazione_nuova > 0) {
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'dsubices');
                    $user2->addChild('valore', getLocation($value->codice_ente, $value->locazione_nuova));
                }
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'nstaccoes');
                $user2->addChild('valore', $value->ubicazione_nuova);
                if ($value->codice_ente == 29) {
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'cmares');
                    $user2->addChild('valore', '004');
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'nmates');
                    $user2->addChild('valore', $value->matricola_nuova);
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'diames');
                    $user2->addChild('valore', 13);
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'ncifes');
                    $user2->addChild('valore', 5);
                } else {
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'cmares');
                    $user2->addChild('valore', ' ');
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'nmates');
                    $user2->addChild('valore', ' ');
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'diames');
                    $user2->addChild('valore', $value->portata_nuova);
                    $user2 = $user1->addChild('parametro');
                    $user2->addChild('nome', 'ncifes');
                    $user2->addChild('valore', $value->cifre_nuove);
                }
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'valletes');
                $user2->addChild('valore', $value->lettura);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'vallet1es');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'cdes');
                $user2->addChild('valore', ' ');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'nfunz');
                $user2->addChild('valore', 'MWM');
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'DSSTATO');
                $user2->addChild('valore', $value->profilo_di_prelievo);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'annot_val');
                $user2->addChild('valore', $value->indirizzo_nuovo);
                $user2 = $user1->addChild('parametro');
                $user2->addChild('nome', 'rich_val');
                $user2->addChild('valore', $value->flag_contatore_padre);
            }
            header('download: ok');
            echo $xml->asXML();
            if (trim($codice_int) == '') {
                Letture::whereIn('codice_ente', $enti)
                    ->where('stato', 'LET')
                    ->where('flag_controllata', 'O')
                    ->update(['stato' => 'SPE']);
            } else {
                Letture::whereIn('codice_ente', $enti)
                    ->where('stato', 'LET')
                    ->where('flag_controllata', 'O')
                    ->where('pdr', doubleval($codice_int))
                    ->update(['stato' => 'SPE']);
            }
        } else {
            header('download: error');
            echo "Error in result query oppure nessun record da scaricare";
        }






    }


}
