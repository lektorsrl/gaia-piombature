<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letture_dettaglio extends Model
{
    protected $table = 'letture_dettaglio';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
