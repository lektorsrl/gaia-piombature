<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locazioni extends Model
{
    protected $table = 'locazioni';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
