<?php

use App\Locazioni;

if (! function_exists('FormatStr')) {
    function FormatStr($s, $tipo, $lung) {
        $stringazza="                                                                                                                                                                ";
        $numerazzo="00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        $risultato=$s;
        switch ($tipo){
            case "SD": $risultato = substr($s.$stringazza, 0, $lung); break;
            case "SS": $risultato = substr($stringazza.$s, (-1)*$lung); break;
            case "N":  $risultato = substr($numerazzo.$s, (-1)*$lung); break;
        }
        return $risultato;
    }
}

if (! function_exists('StripTrim')) {
    function StripTrim($s){
        $risultato = trim(str_replace("'","''",stripslashes($s)));
        return $risultato;
    }
}

if (! function_exists('checkAutorization')) {
    function checkAutorization($user, $pass) {
        if($user == 'gaia' and $pass == 'PwdG41a'){
            return true;
        }else{
            return false;
        }
    }
}

if (! function_exists('getLocation')) {
    function getLocation($ente, $id) {
        $codice = Locazioni::where('ente_id',$ente)
                            ->where('id',$id)->pluck('codice');
        return $codice[0];
    }
}



