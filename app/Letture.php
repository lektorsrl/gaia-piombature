<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letture extends Model
{
    protected $table = 'letture';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
